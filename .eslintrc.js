module.exports = {
    "env": {
        "amd": true
    },
    "extends": "airbnb",
    "rules": {
      "semi": [2, "always"]
    }
};