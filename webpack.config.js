var LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
  },
  devServer: {
    index: 'index.html',
    watchContentBase: true,
  },
  watch: true,
  plugins: [
    new LiveReloadPlugin({}),
  ],
};
