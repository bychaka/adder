# Adder

This project allows you to sum up numbers using the closure function.

  `sum(1)();        // 1`
  
  `sum(2)(2)(2)();  // 6`

## Install project

- Install [Node.js](https://nodejs.org/en/)
- Run `npm install` in command line

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:8080/`. The app will automatically reload if you change any of the source files.


