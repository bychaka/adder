import { isNumber } from "util";

function sum(x) {
  let result = x;
  
  function up(y) {
    if (!isNumber(y)){
      y = 0;
    }
    result +=y
    return up; 
  }

  up.toString = function() {
    return result;
  };

  return up;
}

console.log(sum(5)(88)(8)(6)());